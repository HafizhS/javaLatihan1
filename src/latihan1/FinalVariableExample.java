package latihan1;

public class FinalVariableExample {
	
	
	private final int finalVar = 2; // <== final mean Just once Assigned
	private void changeFinalVar() {
		System.out.println("Latihan finalVariable1");
		System.out.println(finalVar);
		//finalVar = 0; <== Cannot assgined again
	}
	
	public static void main(String[] args) {	
		new FinalVariableExample().changeFinalVar();
	}
}
