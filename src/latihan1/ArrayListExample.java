package latihan1;

import java.io.Console;
import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		
		//create an ArrayList
		ArrayList al = new ArrayList();
		System.out.println("Initial size of al: " + al.size());
				
		//Array Start at Zero
		//add elements to the ArrayList
		al.add("C");
		al.add("A");
		al.add("E");
		al.add("B");
		al.add("D");
		al.add("F");
		al.add(1,"A2"); // <== add elements to index 1
		System.out.println("Size of al after Additions :" + al.size());
		
		//display the ArrayList
		System.out.println("Contents of al: " + al);
		
		//remove elements from the ArrayList
		al.remove("F"); // <== delete Array contain F
		al.remove(2); // <== delete Array Index 2
		System.out.println("Size of al after Deletions: " + al.size());
		System.out.println("Contents of al: " + al);
	}
}
