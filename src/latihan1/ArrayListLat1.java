package latihan1;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListLat1 {

	private static boolean mode = true;
 	public static void main(String[] args) {
		do {
			//Membuat Objek ArrayList
			ArrayList al = new ArrayList();
			System.out.println("Size ArrayList Pertama: " + al.size());
			
			//Membuat Objek Scanner
			Scanner scn = new Scanner(System.in);
			int jumlahInput;
			int input;
			
			//Deklarasi jumlah input
			System.out.println("Jumlah element yang ingin di Masukkan: ");
			jumlahInput = scn.nextInt();
			for (int i = 0;i < jumlahInput;i++)
			{
				//Add nilai index dengan Scanner sesuai Jumlah inputan
				System.out.println("Masukkan Nilai Element index "+i+": ");
				input = scn.nextInt();
				al.add(input);
			}
			
			//display ArrayList
			System.out.println("Size ArrayList Kedua: " + al.size());
			System.out.println("Display Element ArrayList: " + al);

			//pengulangan
			System.out.println("Ingin MenguLangi [Y/N] : ");
			char loop = new Scanner(System.in).next().charAt(0);
			if (loop == 'N' || loop == 'n')
			{
				mode = false;
			}
			input = 0;
			jumlahInput = 0;
			System.out.println("");;
			}while(mode);
		 	System.out.println("Selesai!");
		}
}
