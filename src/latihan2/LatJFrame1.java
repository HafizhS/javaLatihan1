package latihan2;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

public class LatJFrame1{
	JButton jButton;
	JFrame jFrame;
	JTextField jTextField;
	JLabel jLabel;
	ActionListener button_actionListener;
	
	public LatJFrame1() {		
		jFrame = new JFrame();
		jButton = new JButton();
		jTextField = new JTextField();
		jLabel = new JLabel();
		button_actionListener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				jLabel.setText(jTextField.getText());
				
			}
		};
		
		jFrame.setVisible(true); // <= Visible to JFrame. in C# it means : Form1.Enabled = true;
		jFrame.setSize(400, 300);
		jFrame.setTitle("Latihan JFrame");
		jFrame.setResizable(false); // <= Cant Resize Form
		jFrame.add(jButton); // <= Add Component(Object) to JFrame
		jFrame.add(jTextField);
		jFrame.add(jLabel);
		jFrame.setLayout(null); // <= Layout
		jFrame.setLocationRelativeTo(null); // <= Dunno but Important
		
		jButton.setText("Lat1 Button");
		jButton.setBounds(120, 10, 100, 40); // X,Y,Height,Width
		jButton.addActionListener(button_actionListener);
		
		jTextField.setVisible(true);
		jTextField.setText("Lat1 TextField");
		jTextField.setBounds(10,10,100, 40);
		
		jLabel.setVisible(true);
		jLabel.setText("Lat1 Label");
		jLabel.setBounds(10,60,100,40);
		
//		System.out.println(jButton.getBounds());
//		System.out.println(jFrame.getSize());
//		System.out.println(jButton.getSize());
		
	}
	public static void main(String[] args) {
		new LatJFrame1();
		
	}
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
