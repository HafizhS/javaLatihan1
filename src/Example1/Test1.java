package Example1;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

import javax.swing.Timer;

import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Test1 implements ActionListener {
	
	public static Test1 test1; // <== make this Test1 to Static (test1)
	
	JFrame jFrame;
	renderTest1 renderer; //this class contain static Test1
	Timer timer;
	
	private final int Height = 600;
	private final int Width = 800;
	
	private void init()	{
		renderer = new renderTest1();
		jFrame = new JFrame();
		timer = new Timer(20,this);
	}
	
	public Test1() {
		init();
		jFrame.setVisible(true);
		jFrame.setResizable(false);
		jFrame.setTitle("Test1");
		jFrame.setSize(Width, Height);
		jFrame.add(renderer);
		timer.start();
	}
	private int X = Width/2;
	private int Y = Height/2;
	public void repaint(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, Width, Height);
		g.setColor(Color.black);
		g.fillRect(X,Y,30,30); // X,Y,Width,Height not X1,Y2,X2,Y2
		System.out.println(g.getClipBounds());
	}
	public static void main(String[] args) {
		
		test1 = new Test1(); // <== use the Static one
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(X > Width)
		{
			X = 10;
		}
	}
}
