package latihan3;

import javax.crypto.Cipher;

import java.sql.*;
import javax.crypto.spec.SecretKeySpec;

public class cryptLatihan {
	
	
	private void start() throws Exception {
		String wannaEcypted = "Hafizh";
		String privateKey = "112";
		
		String cryptedText = crypt(wannaEcypted, privateKey);
		String deCrytedText = deCrypt(cryptedText, privateKey);
		System.out.println(cryptedText);
		System.out.println(deCrytedText);
	}
	
	private String crypt(String clearText,String privateKey) throws Exception {
		String strData = "";
		try {
			SecretKeySpec skeyspec = new SecretKeySpec(privateKey.getBytes(),"Blowfish");
			Cipher cipher= Cipher.getInstance("Blowfish");
			cipher.init(cipher.ENCRYPT_MODE, skeyspec);
			byte[] encrypted = cipher.doFinal(clearText.getBytes());
			strData = new String(encrypted);
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return strData;
	}
	
	private String deCrypt(String encryptedText,String privateKey) throws Exception {
		String strData = "";
		try {
			SecretKeySpec skeyspec = new SecretKeySpec(privateKey.getBytes(), "Blowfish");
			Cipher cipher = Cipher.getInstance("Blowfish");
			cipher.init(cipher.DECRYPT_MODE, skeyspec);
			byte[] decrypted = cipher.doFinal(encryptedText.getBytes());
			strData = new String(decrypted);
		}catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}
		return strData;
	}

	public static void main(String[] args) throws Exception {
		new cryptLatihan().start(); 
	}
}
