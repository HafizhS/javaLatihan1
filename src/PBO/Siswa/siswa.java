package PBO.Siswa;

public class siswa {
	private String nama;
	private String mataPelajaran;
	private float nilai;
	private String kriteriaNilai;

	public siswa(String nama,String mataPelajaran) {
		this.nama = nama;
		this.mataPelajaran = mataPelajaran;
	}

	public siswa() { }

	public String getNama() { return nama; }

	public String getMataPelajaran() { return mataPelajaran; }
	
	public float getNilai() { return nilai; }
	
	public String getKriteriaNilai() { return kriteriaNilai; }

	public void setNama(String value) { nama = value; }

	public void setMataPelajaran(String value) { mataPelajaran = value; }

	public void setNilai(float value) { nilai = value; }
	
	public void setKriteriaNilai(String value) { kriteriaNilai = value; }
		
}
