package PBO.Siswa;

public class nilai {
	private siswa _siswa;
	final private String[] nilaiKriteria = {"A","B","C","D","E","Gagal"};
	
	public nilai(siswa siswa) {
		this._siswa = siswa;
	}
	
	public void seleksiNilai() {
		if (_siswa.getNilai() < 50) {
			_siswa.setKriteriaNilai(nilaiKriteria[5]);
		} else if (_siswa.getNilai() <= 59) {
			_siswa.setKriteriaNilai(nilaiKriteria[4]);
		} else if (_siswa.getNilai() <= 69) {
			_siswa.setKriteriaNilai(nilaiKriteria[3]);
		} else if (_siswa.getNilai() <= 79) {
			_siswa.setKriteriaNilai(nilaiKriteria[2]);
		} else if (_siswa.getNilai() <= 89) {
			_siswa.setKriteriaNilai(nilaiKriteria[1]);
		} else if (_siswa.getNilai() <= 100) {
			_siswa.setKriteriaNilai(nilaiKriteria[0]);
		} else if (_siswa.getNilai() > 100) {
			System.out.println("Batas Nilai sampai 100");
		}
		
//		90 100 a
//		80 89 b 
//		70 79 c 
//		60 69 d
//		50 59 e
//		gagal
	}
}
