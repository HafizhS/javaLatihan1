package PBO.Siswa;

import java.util.Scanner;

public class mainApp {
	
//	private boolean isOutValue() {
//		if (siswa1.getKriteriaNilai() != null) {
//			return false;
//		} else {
//			return true;
//		}
//	}
	
	private void start( ) {
		Scanner scn = new Scanner(System.in);
		siswa siswa1 = new siswa();
		nilai nilai1 = new nilai(siswa1);
		
		System.out.print("Masukkan Nama Siswa : "); 
		siswa1.setNama(scn.nextLine());
		System.out.print("Masukkan Mata Pelajaran : ");
		siswa1.setMataPelajaran(scn.nextLine());
		System.out.print("Masukkan nilai Mata Pelajaran "+siswa1.getMataPelajaran()+" : ");
		siswa1.setNilai(scn.nextFloat());
		
		System.out.println("===============================");
		System.out.println("Nama Siswa \t: "+siswa1.getNama());
		System.out.println("Mata Pelajaran \t: "+siswa1.getMataPelajaran());		
		System.out.println("Nilai Mapel \t: "+siswa1.getNilai());
		System.out.println("===============================");
		nilai1.seleksiNilai();
		if (siswa1.getKriteriaNilai() != null) {
			System.out.println("Kriteria Nilai \t: "+siswa1.getKriteriaNilai());
		}
	}
	
	public static void main(String[] args) {
		new mainApp().start();
	}
}
