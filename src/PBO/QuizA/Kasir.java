package PBO.QuizA;

import java.util.Scanner;

public class Kasir {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		Makanan mkn = new Makanan();
		mkn.showMakanan();
		System.out.print("Pilih makanan: ");
		int pilih = scn.nextInt();
		System.out.print("Jumlah Beli: ");
		int jumlahItem = scn.nextInt();
		System.out.print("Member? true/false: ");
		boolean member = scn.nextBoolean();
		System.out.println("-----Struk-----");
		int hargaItem = mkn.hargaMakanan[pilih-1];
		double jumlahHarga = 0;
		int diskon; 
		if (member) {
			jumlahHarga = (hargaItem * jumlahItem) * (mkn.diskonMakanan[pilih-1]/100);
		}else if (!member) {
			jumlahHarga = (hargaItem * jumlahItem);
		}
		System.out.println(mkn.makanan[pilih-1]+" \t"+jumlahItem+" \t"+jumlahHarga);
		System.out.print("Masukkan uang anda: ");
		double uang = scn.nextInt();
		double kembalian;
		if (uang == jumlahHarga) {
			System.out.println("Uang anda Pas!");
		}else if (uang < jumlahHarga) {
			System.out.println("Uang anda Kurang!");
		}else {
			kembalian = uang - jumlahHarga;
			System.out.println("Kembalian anda: "+kembalian);
		}
		System.out.println("-----Terima Kasih-----");
	}
}