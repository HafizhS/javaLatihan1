package PBO.QuizA;

public class Makanan {
	
	public String[] makanan = {"Bala-Bala","Gehu","Seblak"};
	public int[] hargaMakanan = {500,10000,6000};
	public double[] diskonMakanan = {5,20,15};
	public void showMakanan() {
		System.out.println("-----MenuMakanan-----");
		for (int i = 0; i < makanan.length; i++) {
			System.out.println((i+1)+". "+makanan[i]+" \t"+hargaMakanan[i]);
		}
		System.out.println("---------------------");
	}
}
