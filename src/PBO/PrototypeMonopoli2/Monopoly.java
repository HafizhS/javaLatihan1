package PBO.PrototypeMonopoli2;

import java.util.Scanner;

public class Monopoly extends LahanList {
	
	Dadu[] dadu;
	Pemain[] player;
	private int totalPlayer;
	boolean isGameEnd = false;
	boolean hasPlayer = false;
	private int[] daduFace;
	public int currentPlayer = 0;
	private int dice_flag;
	private boolean double_kesempatan = false;
	private boolean triple_penalty= false;
	private Scanner scn1 = new Scanner(System.in);
	private final int uangMulai = 150000;
	public boolean hasWinner = false;
	private boolean debugMode = false;
	
	
	Monopoly(int Player) {
		hasPlayer = true;
		totalPlayer = Player;
		init();
	}
	
	
	private void init() {
		dadu = new Dadu[2];
		for (int i = 0; i < dadu.length; i++) {
			dadu[i] = new Dadu();
		}
		daduFace = new int[dadu.length];
		player = new Pemain[totalPlayer];
		for (int i = 0; i < player.length; i++) {
			player[i] = new Pemain();
			player[i].setNama(""+(i+1));
			player[i].setUang(uangMulai);
			if (debugMode) {
				System.out.println("Player["+(i+1)+"] diberi Uang sebanyak: "+uangMulai+"("+player[i].getUniqueId()+" / "+player[i]+")");
			}
		}
		currentPlayer = 0;
		
		if (debugMode) {
			lahan[11].setOwner(player[1]);
			System.out.println("lahan 11: "+lahan[11].getOwner().getUniqueId()+" / "+lahan[11].getOwner());
		}
		
	}
	
	public int getDice_flag() {	return this.dice_flag; }
	public boolean getDouble_State() { return this.double_kesempatan; }
	public void setDouble_State(boolean value) { this.double_kesempatan = value; }
	public void setDebugMode(boolean value) { this.debugMode = value;}
	
	public void movePlayer(int player,int diceValue) {
		currentPlayer = player;
		int newPos = modPos(getCurrentPemain().current_Pos + diceValue);
		getCurrentPemain().setPos(newPos);
		System.out.println("[Player "+(currentPlayer+1)+"] [TURN: "+getCurrentPemain().getTurn()+"]"+" bergerak sebanyak: "+diceValue+"\n Posisi player berada di: "+getCurrentPemain().current_Pos);
		if (double_kesempatan) {
			System.out.println("DOUBLE KESEMPATAN");
		}
		for (int i = 0; i < lahan.length;i++) {
			if (lahan[i].getPosLahan() == getCurrentPemain().current_Pos) {
				System.out.println("  Anda menginjak: "+lahan[i].getNamaLahan());
				buySelection(getCurrentPemain(), lahan[i]);
			}
		}
		System.out.println("=========================================");
	}
	
	public void buySelection(Pemain player,Lahan lahan) {
		if (lahan.getOwner() == null) {
			if (player.getUang() >= lahan.getHargaLahan()) {
				char selection = '\0';
				System.out.println("   Uang anda Sekarang: "+player.getUang());
				System.out.println("   Apakah anda ingin membeli Lahan: "+lahan.getNamaLahan()+"["+lahan.getPosLahan()+"] [Y/N]");
				System.out.println("    Harga Lahan: "+lahan.getHargaLahan());
				selection = scn1.next().charAt(0);
				if (selection == 'Y' || selection == 'y') {
					player.setUang(-lahan.getHargaLahan());
					lahan.setOwner(player);
					System.out.println("Tanah ini sekarang milik: "+lahan.getOwner().getNamaPlayer()+" ("+lahan.getOwner().getUniqueId()+" / "+lahan.getOwner()+")");
					System.out.println("Sukses membeli tanah"+", Sisa uang anda: "+player.getUang());
				} else if (selection == 'N' || selection == 'n') {
					System.out.println("Anda tidak membeli tanah ini");
				} else if (selection == '\0') {
					return;					
				}
			}else if (player.getUang() < lahan.getHargaLahan()) {
				System.out.println("Anda tidak mempunyai uang yang cupuk");
//				pinjamBank();
			}
		}else if (lahan.getOwner() != player)	{
			System.out.println("Anda menginjak tanah lawan :"+lahan.getOwner().getNamaPlayer()+"("+lahan.getOwner().getUniqueId()+" / "+lahan.getOwner()+")");
			final int hargaSewa = lahan.getSewaLahan();
			if (player.getUang() < hargaSewa) {
				System.out.println("Anda tidak mempunyai uang yang cukup");
//				pinjamBank();
			}else {
				player.setUang(-hargaSewa);
				lahan.getOwner().setUang(+hargaSewa);
				System.out.println("Uang anda Dikurangi: "+hargaSewa+", Uang anda Sekarang: "+player.getUang());
			}
		}else if (player == lahan.getOwner()) {
		System.out.println("Anda menginjak tanah sendiri");
		}
	}
	
	public void showLahanList() {
		for (int i = 0; i < lahan.length; i++) {
			System.out.println("Lahan: ["+lahan[i].getPosLahan()+"]"+lahan[i].getNamaLahan());
		}
		System.out.println("==================================\n");
	}
	
	public Pemain[] getPemain() {
		return player;
	}
	
	public Pemain getCurrentPemain() {
		return player[currentPlayer];
	}
	
	public int rollDadu() {
		int totalValue = 0;
		int[] dVal = new int[dadu.length];
		for (int i = 0;i < dadu.length;i++) {
			dVal[i] = dadu[i].getDaduFace();
			System.out.println("Dadu ke "+(i+1)+": "+dVal[i]+" ");
			totalValue += dVal[i];
		}
		for (int i = 1;i < dVal.length;i++) {
			if (dVal[i] == dVal[i-1]) { double_kesempatan = true; }
		}
		return totalValue;
	}
	
	public int modPos(int _currentPos) {
		return _currentPos % lahan.length;
	}
	
	public int normalizePlayer(int _currentPlayer) {
		return currentPlayer % player.length;
	}
}