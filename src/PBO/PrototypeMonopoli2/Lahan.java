package PBO.PrototypeMonopoli2;

public class Lahan {
	private String namaLahan;
	private int posLahan;
	private int hargaLahan;
	private int sewaLahan;
	private Pemain owner = null;
	
	Lahan() {}
	Lahan(String namaLahan,int posLahan,int hargaLahan,int sewaLahan) {
		this.namaLahan = namaLahan;
		this.posLahan = posLahan;
		this.hargaLahan = hargaLahan;
		this.sewaLahan = sewaLahan;
	}
	
	//set
	public void setNamaLahan(String value) { this.namaLahan = value; }
	public void setPosLahan(int value) { this.posLahan = value; }
	public void setHargaLahan(int value) { this.hargaLahan = value; }
	public void setSewaLahan(int value) { this.sewaLahan = value; }
	public void setOwner(Pemain value) { this.owner = value; }
	
	//get
	public String getNamaLahan() { return namaLahan; }
	public int getPosLahan() { return posLahan; }
	public int getHargaLahan() { return hargaLahan; }
	public int getSewaLahan() { return sewaLahan; }
	public Pemain getOwner() { return owner; }
	public int getOwnerHashCode() { return owner.hashCode(); }
}
