package PBO.PrototypeMonopoli2;

import java.util.Random;

public class Dadu {
	private final int totalKesempatan = 6;
	Random random = new Random();
	
	public int getDaduFace() {
		int face = 1 + random.nextInt(totalKesempatan);
		return face;
	}
}