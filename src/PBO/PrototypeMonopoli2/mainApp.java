package PBO.PrototypeMonopoli2;

import java.util.Scanner;

public class mainApp {
	
	Monopoly monopoly;
	mainApp(int _player) {
		monopoly = new Monopoly(_player);
	}
	
	public void startGame() { 
		
		Scanner scn1 = new Scanner(System.in);
		monopoly.showLahanList();
		
		System.out.println(monopoly.hashCode());
		
		System.out.println("===============================================");
		System.out.println("Jumlah Pemain: "+monopoly.getPemain().length+"");
		System.out.println("Hashcode (Id) Setiap Pemain: ");
		for (int i = 0; i < monopoly.getPemain().length;i++) {
			System.out.println("[Player "+(i+1)+"] :"+monopoly.getPemain()[i].getUniqueId() + " / " + Integer.toHexString(monopoly.getPemain()[i].hashCode()));
		}
		System.out.println("===============================================\n");
		
		while (!monopoly.hasWinner && !monopoly.isGameEnd) {
		monopoly.movePlayer(monopoly.currentPlayer, monopoly.rollDadu());
		if (monopoly.getDouble_State()) {
			monopoly.setDouble_State(false);
//			System.out.println("DOUBLE KESEMPATAN"); 
			monopoly.movePlayer(monopoly.currentPlayer, monopoly.rollDadu());
			}
		monopoly.getCurrentPemain().setTurn(+1);
		monopoly.currentPlayer += 1;
		this.monopoly.currentPlayer = this.monopoly.normalizePlayer(monopoly.currentPlayer);
		
		scn1.nextLine();
		}
		
		System.out.println("Game Selesai");
	}
	
	public static void main(String[] args) {
		
		// init jumlahPlayer
		Scanner scn = new Scanner(System.in);
		int _totalPlayer = 0;
		while (_totalPlayer < 2 || _totalPlayer > 4) {
			try {
				System.out.print("Masukkan Banyak Player ( 2 - 4 ): ");
				_totalPlayer = scn.nextInt();
				if (_totalPlayer < 2) {
					System.out.println("Player tidak boleh kurang dari 2");
				}else if (_totalPlayer > 4) {
					System.out.println("Player tidak boleh lebih dari 4");
				}
			}catch (Exception ex) {
				System.err.println("setPlayer Error! ("+ex.getCause()+")");
				System.exit(1);
				break;
			}
		}
		new mainApp(_totalPlayer).startGame();
	}
}

