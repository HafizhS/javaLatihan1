package PBO.PrototypeMonopoli2;

public class Pemain {
	private int Uang;
	private String[] ownPlot;
	public int current_Pos = 0;
	private int keliling;
	private int turn = 1;
	private String namaPlayer = "Player ";
	
	//set
	public void setPos(int _newPos) { this.current_Pos = _newPos; }
	public void setUang(int value) { Uang += value; }
	public void setKeliling(int value) { this.keliling = value; }
	public void setTurn(int value) { this.turn += value; }
	public void setNama(String value) { this.namaPlayer += value; }
	
	//get
	public int getUang() {return this.Uang; }
	public int getCurrentPos() {return this.current_Pos; }
	public int getUniqueId() {return this.hashCode(); }
	public int getTurn() { return this.turn; }
	//public String getHexUniqueId() {return Integer.toHexString(this.hashCode());}
	public Pemain getObj() { return this; }
	public String getNamaPlayer() { return this.namaPlayer; }
	
	public int lemparDadu(Dadu dadu) { return dadu.getDaduFace(); }
	public void addOwnedPlot(Lahan lahan) {
		this.ownPlot[ownPlot.length] += lahan.getNamaLahan();
	}
}
