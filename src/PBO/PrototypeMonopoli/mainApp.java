package PBO.PrototypeMonopoli;

import java.util.Scanner;

public class mainApp extends LahanList{
	
	Dadu[] dadu;
	Pemain[] pemain;
	Scanner scn;
	private final int totalPlot = 12;
	private final int danaAwal = 150000;
	private int kurangDana;
	private int jumlahPemain;
	private boolean playerisSetted;
	
	private void setPemain() {
		jumlahPemain = 0;
		System.out.print("Masukkan Jumlah Pemain (2 - 4) :");
		jumlahPemain = scn.nextInt();
		while (jumlahPemain > 4 || jumlahPemain < 2) {
			System.out.println("Invalid Total Player");
			setPemain();
		}
		playerisSetted = true;
	}
	
	private void init() {
		scn = new Scanner(System.in);
		playerisSetted = false;
		dadu = new Dadu[4];
		for (int i = 0; i <	dadu.length;i++) {
			dadu[i] = new Dadu();
		}
		pemain = new Pemain[2];
		for (int i = 0;i <pemain.length; i++) {
			pemain[i] = new Pemain();
			jumlahPemain =+1;
		} 
		
	}
	
	private boolean debugMode;
	private void setDebugMode(boolean value) {
		this.debugMode = value;
	}
	
	private void showLahan() {
		for (int i = 0; i < lahan.length; i++) {
			System.out.println("Lahan: "+lahan[i].getNamaLahan() +" Berada di Petak no: \t"+(i+1));
		}
	}
	
	boolean double_kesempatan = false;
	int flagDadu = 0;
	private int daduKesempatan() {
		int jumlah_kesempatan = 0;
		int[] a = new int[dadu.length];
		for (int i = 0;i < dadu.length;i++ ) {
			a[i] = 0;
			a[i] = dadu[i].getRandomKesempatan();
			jumlah_kesempatan += a[i];
			if (debugMode)
			System.out.println("Dadu ke "+(i+1)+", Mempunyai Nilai "+a[i]);
		}
		if (debugMode)
			System.out.println("Jumlah Kesempatan "+jumlah_kesempatan);
		
		// check double dice
		// belum menggunakan auto dice
		int x = 0;
		for (int i = 0;i <a.length;i++) {
			if (a[i] == a[-i+1]) {
				if (debugMode) {
					if (flagDadu == 3) {
						System.out.println("Anda Masuk Penjara");
						flagDadu = 0;
					}else {
					a[i] = 0;
					flagDadu += 1;
					double_kesempatan = true;
					setTotalKesempatan(jumlah_kesempatan);
					}
		
				}
			}
		}
		
		return jumlah_kesempatan;
	}
	
	private void setTotalKesempatan(int value) {
		totalKesempatanAll += value;
		value =0;
	}
	int totalKesempatanAll;
	
	private void playerMove(int value,int player) {
		pemain[player].pos += value;
		if (double_kesempatan) {
			System.out.println("DOUBLE KESEMPATAN");
		}
		System.out.println("Pemain "+(player+1)+" bergerak "+value+" Langkah");
		
	}
	
	private void changePlayer() {
		if (finishTurnState())
			System.out.println("Finish");
	}
	
	private boolean _finishTurnState = false;
	private boolean finishTurnState() {
		return true;
	}
	
	private int playerTurn;
	private void start() {
		
		//initialize
		init();
		setDebugMode(true);
		showLahan();
		if (debugMode) {
			for (int i = 0; i < pemain.length;i++) {
				pemain[i].setUang(danaAwal);
				System.out.println("Dana awal pemain "+(i+1)+" Adalah "+danaAwal);
			}
			System.out.println("-------------------\n");
		}
		
		do {
			while (!_finishTurnState) {
					playerMove(daduKesempatan(), 0);
					checkPlayerPos();
					if (double_kesempatan) {
						double_kesempatan = false;
						playerMove(daduKesempatan(), 0);
						checkPlayerPos();
						if (double_kesempatan) {
							double_kesempatan = false;
							playerMove(daduKesempatan(), 0);
							checkPlayerPos();
						}
//						System.out.println("DOUBLE KESEMPATAN");
					}
				_finishTurnState = true;
			}
			playerTurn += 1;
		}while (playerTurn == 0);
	}
	
	private void checkPlayerPos() {
		for (int i = 0; i < lahan.length; i++) {
			if (pemain[0].pos == lahan[i].getPosLahan()) {
				System.out.println("Anda Menginjak "+lahan[i].getNamaLahan()+"\n"+ "dan Anda berada di Posisi: "+pemain[0].pos);
			}	
		}
	}
	
	
	public static void main(String[] args) {
		new mainApp().start();
	}
}

