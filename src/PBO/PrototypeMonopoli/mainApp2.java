package PBO.PrototypeMonopoli;

import java.util.Scanner;

public class mainApp2 extends LahanList {
	
	
	Pemain[] player;
	Scanner scn = new Scanner(System.in);
	private int totalPlayer = 0;
	private boolean hasPlayer = false;
	
	private void init() {
		player = new Pemain[totalPlayer];
	}
	
	
	private void setTotalPlayer() {
		totalPlayer = 0;
		while (totalPlayer < 2 || totalPlayer > 4) {
			try {
				System.out.println("Masukkan banyak Pemain (2 - 4): ");
				totalPlayer = scn.nextInt();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}finally {
				hasPlayer = true;
				if (totalPlayer < 2) {
					System.out.println("Player Kurang dari 2");
					hasPlayer = false;
				}
				else if (totalPlayer > 4) {
					System.out.println("Player Lebih dari 4");
					hasPlayer = false ;
				}
			}	
		}
	}
	
	private void startGame() {
		while(!hasPlayer) {
			setTotalPlayer();
		}
		
		
	}
	
	public static void main(String[] args) {
//		new mainApp2().startGame();
	}
}
