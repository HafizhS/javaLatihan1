package PBO.Makanan;

import java.util.ArrayList;


public class DaftarMakanan {
	
	protected ArrayList<Makanan> daftarMakanan = new ArrayList<Makanan>();
	
	public void addMakanan(Makanan _makanan) {
		daftarMakanan.add(_makanan);
	}

	public void showMakanan() {
		for (int i = 0;	i < daftarMakanan.size();i++ ) {
			System.out.println(i+". "+daftarMakanan.get(i).getNamaMakanan()+" \tHarga : "+daftarMakanan.get(i).getHarga());
		}
	}
	public void deleteMakanan(int value) {
		daftarMakanan.remove(value);
	}
	
	public void callLastElement() {
		System.out.println(daftarMakanan.get(daftarMakanan.size()-1).getNamaMakanan()); 
		System.out.println(daftarMakanan.size());
	}
	
	public ArrayList<Makanan> getArrayListMakanan() {
		return daftarMakanan;
	}
}
