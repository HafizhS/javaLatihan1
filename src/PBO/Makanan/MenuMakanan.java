package PBO.Makanan;

import java.util.Scanner;

public class MenuMakanan {
	
	TambahMakanan tambahMakanan;
	DaftarMakanan daftarMakanan;

	public void start() {
		daftarMakanan = new DaftarMakanan();
		tambahMakanan = new TambahMakanan();
		Pesanan pesan = new Pesanan(daftarMakanan);

		//		daftarMakanan.addMakanan(tambahMakanan.addMakananViaInput());
		
		Makanan gehu = new Makanan(); 
		gehu.setNamaMakanan("Gehu");
		gehu.setHarga(1000);
		daftarMakanan.addMakanan(gehu);
		
		Makanan balaBala = new Makanan();
		balaBala.setNamaMakanan("Bala-Bala");
		balaBala.setHarga(1500);
		daftarMakanan.addMakanan(balaBala);
		
		Makanan cireng = new Makanan();
		cireng.setNamaMakanan("Cireng");
		cireng.setHarga(500);
		daftarMakanan.addMakanan(cireng);
	
		Makanan cipuk = new Makanan();
		cipuk.setNamaMakanan("Cipuk");
		cipuk.setHarga(1500);
		daftarMakanan.addMakanan(cipuk);
		
//		daftarMakanan.callLastElement();
		
//		pesan.setDaftarMakanan(daftarMakanan);
		
		daftarMakanan.showMakanan();
		pesan.pilihMakanan();
		
	}
	
	public static void main(String[] args) {
		new MenuMakanan().start();
	}
}