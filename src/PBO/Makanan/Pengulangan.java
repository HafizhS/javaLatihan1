package PBO.Makanan;

import java.lang.reflect.Constructor;
import java.util.Scanner;

public class Pengulangan {
	private Scanner loopScn = new Scanner(System.in);
	private char loop;
	public void startLoop() {
		System.out.println("Ingin Mengulangi lagi? [Y/N]");
		loop = loopScn.next().charAt(0);
		if(loop == 'y' || loop == 'Y') {
			System.out.flush();
		}else {
			System.out.close();
			System.exit(1);
		}
	}
	
}
