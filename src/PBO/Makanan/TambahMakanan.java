package PBO.Makanan;

import java.util.Scanner;

public class TambahMakanan {
	private DaftarMakanan _daftarMakanan;
	private Makanan _makanan;
	private Scanner scn = new Scanner(System.in);
	
	public Makanan addMakananViaInput() {
		String namaMakanan = "";
		int Harga = 0;
		_makanan = new Makanan();
		
		System.out.print("Masukkan nama makanan : ");
		namaMakanan = scn.nextLine();
		System.out.print("Masukkan harga : ");
		Harga = scn.nextInt();
		
		_makanan.setNamaMakanan(namaMakanan);
		_makanan.setHarga(Harga);
		
		return _makanan;
	}
}
