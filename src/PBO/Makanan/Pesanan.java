package PBO.Makanan;

import java.util.Scanner;

public class Pesanan{
	
	private DaftarMakanan _daftarMakanan;
	private Scanner scn = new Scanner(System.in);
	private int pilihan;
	private int totalHarga;
	private int totalBarang;
	private int uangBayar;
	private int uangKembalian;
	private int uangKurang;
	private boolean isOutRange;
	
	
	public Pesanan(DaftarMakanan value) {
		_daftarMakanan = value;
	}
	
	public void pilihMakanan() {
		System.out.print("\nPilih Makanan : ");
		pilihan = scn.nextInt();
		
		if (pilihan >= _daftarMakanan.daftarMakanan.size()) {
			System.out.println("Pilihan ke "+pilihan+" Tidak ada");
			isOutRange = true;
		}else {
			isOutRange = false;
		}
		
		if (!isOutRange) {
			System.out.print("Total yang ingin di Beli : ");
			totalBarang = scn.nextInt();
			totalHarga = _daftarMakanan.daftarMakanan.get(pilihan).getHarga() * totalBarang;
			System.out.println("Anda membeli "+_daftarMakanan.daftarMakanan.get(pilihan).getNamaMakanan()+
					", Sebanyak : "+totalBarang+", dengan Total Harga : "+totalHarga );
			pembayaran();
		}
	}
	
	public void pembayaran() {
		System.out.print("\nMasukkan uang yang akan anda Bayarkan : ");
		uangBayar = scn.nextInt();
		if (uangBayar < totalHarga) {
			uangKurang = uangBayar - totalHarga;
			System.out.println("Uang anda Kurang : "+uangKurang);
		}else if (uangBayar == totalHarga) {
			System.out.println("Uang anda PAS, tidak ada kembalian");
		}else {
			uangKembalian = uangBayar - totalHarga;
			System.out.println("Uang anda "+uangBayar+" membayar "+totalHarga+" kembalian "+uangKembalian);
		}
	}
	
	public void setDaftarMakanan(DaftarMakanan value) {
		_daftarMakanan = value;
	}
}